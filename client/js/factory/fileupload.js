app.factory('fileupload', ['Upload','$rootScope','dialogs','$rootScope',function(Upload,$rootScope,dialogs,$rootScope) {
    return {

    	uploadfile : function(file, place,cb){
            console.log('file is0',file);
            if(file.length >1){
                console.log('multi');
            var serverurl = $rootScope.base_path +'/multiUpload/'+place;
            file.upload = Upload.upload({
                url: serverurl,
                arrayKey: '',
                data: {file: file},
            });
            }
            else{         
            console.log('single');       
            var serverurl = $rootScope.base_path +'/upload/'+place;
	        file.upload = Upload.upload({
	            url: serverurl,
                arrayKey: '',
	            data: {file: file},
	        });
            }
            file.upload.then(function (response) {
                return cb(null,response.data);
            }, function (err) {
                return cb("File not uploaded due to some error",null);
            }, function (evt) {
            });
        }
/*        uploadfiles : function(file, place,cb){
            // console.log('file',file);
            var serverurl = $rootScope.base_path +'/multiUpload/'+place;
            file.upload = Upload.upload({
                url: serverurl,
                arrayKey: '',
                data: {file: file},
            });
            
            file.upload.then(function (response) {
                console.log('paths is',response);
                return cb(null,response.data);
            }, function (err) {
                return cb("File not uploaded due to some error",null);
            }, function (evt) {
            });
        }*/

    };

}])
