app.service('comments', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){

	this.getallCommentsCount = function(filters,cb){
		dataService.get('/api/comments/all/count',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getallComments = function(filters , cb){
		dataService.get('/api/comments/all',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.deleteComment = function(uid,cb){
		dataService.delete('/api/comments/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getComments=function(uid,cb){
		dataService.get('/api/comments/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})		
	}
	this.updateComment = function(id, comments,cb){
		dataService.put('/api/comments/'+id,comments,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.createComment = function(data,cb){
		dataService.post('/api/comments/create',data,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}	




















/*	this.createJournal = function(journal,cb){
		dataService.post("/api/journals/create",journal,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}*/
	







}]);