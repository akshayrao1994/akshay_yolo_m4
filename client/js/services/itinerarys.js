app.service('itinerarys', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){
	this.getDistance = function(query , cb){
		dataService.get("/api/itinerarys/distance",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getDayDistanceTime = function(day , cb){
		dataService.post("/api/itinerarys/daydistancetime",{day : day},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getAllModes = function(attraction, cb){
		dataService.post("/api/itinerarys/getallmodes",{attraction : attraction},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.createItinerary =function(itinerary,cb){
		dataService.post("/api/itinerarys/create",itinerary,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getItinerary =function(machine_name,id,cb){
		dataService.get("/api/itinerarys/"+machine_name+"/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.updateItinerary =function(id,json,cb){
		dataService.put("/api/itinerarys/"+id,json,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.deleteItinerary =function(id,cb){
		dataService.delete("/api/itinerarys/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getMap =function(day ,cb){
		dataService.post("/api/itinerarys/route-optimizer",{day : day},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
}]);