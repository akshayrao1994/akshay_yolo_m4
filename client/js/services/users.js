app.service('users', ['$rootScope','dataService','localStorageService','$rootScope', function($rootScope,dataService,localStorageService,$rootScope){
	this.signup = function(json, cb){
		dataService.post('/api/users/register',json,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);
		})
	}
	this.verifyuser = function(url ,cb){
		dataService.post(url,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);
		})
	}

	this.login = function(json, cb){
		dataService.post('/api/users/login',json,function(err,result){
			console.log(result);
			if(err){return cb(err,null)}
			return cb(null,result);
		})
	}
	this.fblogin = function(cb){
		dataService.get('/api/users/login/facebook',{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);
		})
	}
	this.logout = function(cb){
		dataService.get('/api/users/logout',{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.islogin = function(){
		dataService.get('/api/users/islogin',{},function(err,result){
			if(result == null){
				localStorageService.remove('yolo_itinerary_client');
			}else{
				localStorageService.set('yolo_itinerary_client',result);
				$rootScope.loggedInUser = result;
			}
		})
	}
	this.isloginwithcb = function(cb){
		dataService.get('/api/users/islogin',{},function(err,result){
			if(result == null){
				localStorageService.remove('yolo_itinerary_client');
				cb(true,null);
			}else{
				localStorageService.set('yolo_itinerary_client',result);
				$rootScope.loggedInUser = result;
				cb(null,true);
			}
		})
	}
	this.updateuser = function(id, user,cb){
		dataService.put('/api/users/'+id,user,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getalluserscount = function(filters,cb){
		dataService.get('/api/users/all/count',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getallusers = function(filters , cb){
		dataService.get('/api/users/all',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	// this.getallusers = function(filters , cb){
	// 	dataService.get('/api/users/all',filters,function(err,result){
	// 		if(err){return cb(err,null)}
	// 		return cb(null,result);			
	// 	})
	// }
	this.createUser=function(user,cb){
		dataService.post('/api/users/create',user,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getUser=function(uid,cb){
		dataService.get('/api/users/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})		
	}
	this.deleteUser= function(uid,cb){
		dataService.delete('/api/users/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		});	
	}
	this.forgotPassword= function(email,cb){
		dataService.post('/api/users/forgot/password/'+email,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		});	
	}
	this.saveNewPassword = function(email,token,password,cb){
		dataService.put('/api/users/'+email+"/"+token,{password : password},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		});	
	}
	this.udpatePassword = function(user,cb){
		console.log(user);
		dataService.put('/api/users/change/password',user,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);
		});
	}
}])