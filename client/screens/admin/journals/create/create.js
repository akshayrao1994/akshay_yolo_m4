app.controller('journalsCreateCtrl', ['$scope','attractions','categories','destinations','journals','tags','utils','$state','dialogs','fileupload', function($scope,attractions,categories,destinations,journals,tags,utils,$state,dialogs,fileupload){
	$scope.publish = [true,false];
	$scope.journal ={};

	$scope.config = {
	'fields' : {'name' : 1},
	'filter':{},
	'sort' :{'name' :1},
	};

	// utils.getAllLanguages(function(err,result){
	// 	if(err){console.log("getAllLanguages",err);}
	// 	$scope.languages = result;		
	// })0

	$scope.getTags = function(query){
     var regex = new RegExp(query, 'gi');
     return _.filter($scope.tag, function(obj){ return obj.name.match(regex);});
    }


    $scope.languages = {"zn":"Chinese","en":"English"};

	$scope.language_change = function(){
		$scope.journal.language = $scope.languages[$scope.journal.language_code];
	}

	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})

	tags.getallTags($scope.config, function(err,result){
		if(err){console.log("getalltags",err);}
		$scope.tag = result;
/*		$scope.tags =[{name:"1111"},{name :"22222"}];*/
		return;
	})

    $scope.cancel = function(){
        $state.go('admin.journals');
    }

	$scope.createJournal = function(){		
		if($scope.CreateJournalForm.$valid)
		{
			$scope.error = false;
			fileupload.uploadfile($scope.temp_file,'journals',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else
				{
					$scope.journal.image_url = result;
					journals.createJournal($scope.journal ,function(err,result){
						if(err)
						{
							if(err.message)
							{
								return $scope.error = err.message;
							}
							else
							{
								return dialogs.openErrorDialog(err);
							}
						}
						else
						{
							dialogs.openMessageDialog("Journal Created");
							$state.go("admin.journals");
						}
					})
				}
			});
		}
		else
		{
			$scope.submitted = true;
		}
	}

}]);