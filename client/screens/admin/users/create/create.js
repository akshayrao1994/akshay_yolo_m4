app.controller('usersCreateCtrl', ['$scope','users','dialogs','$state','$rootScope','fileupload', function($scope, users,dialogs,$state,$rootScope,fileupload){
	$scope.createUser = function(){
		if($scope.user.local.password == $scope.user.local.confirmPassword){
		   $scope.user.verified	=true;

			if($scope.temp_file){
				fileupload.uploadfile($scope.temp_file,'users',function(err,result){
					if(err){ console.log("file not uploaded",err);}
					else{
						$scope.user.image = result;
						users.createUser($scope.user,function(err,result){
			    			if (err) { 
				            	if(err.message){
				            		$scope.error = err.message
				            	}else{
						           dialogs.openErrorDialog(err)
				            	}
				            	return;
				            }else{
				           		dialogs.openMessageDialog('User Created Successfully');
								$state.go('admin.users');
							}
						})
					}

				})
			}else{
				users.createUser($scope.user,function(err,result){
	    			if (err) { 
		            	if(err.message){
		            		$scope.error = err.message
		            	}else{
				           dialogs.openErrorDialog(err)
		            	}
		            	return;
		            }else{
		           		dialogs.openMessageDialog('User Created Successfully');
						$state.go('admin.users');
					}
				})
			}
		}else{
			$scope.error = 'Password doesn\'t match ';
		}
	}
}]);