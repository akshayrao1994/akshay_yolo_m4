app.controller('adminUsersCtrl', ['$scope','users','$state','dialogs','$rootScope','uiGridConstants', function($scope, users,$state,dialogs,$rootScope,uiGridConstants){
	

//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

// 	//get fields for showing
	$scope.config = {
	'fields' : {'profile_fields.first_name' : 1,'local.email' : 1,'last_login':1},
	'filter':{issuperadmin : false},
		'skip' : 0,
		'limit' : $scope.limit,
	};
	$scope.user = {};
	$scope.user.local ={username : '',email:''};
   
	//grid fields definition
	$scope.usersGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "First Name" , field : "profile_fields.first_name"},
		{displayName : "Email" , field : "local.email"},
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-click="grid.appScope.edituser(row.entity)" class="btn btn-xs btn-warning table_btns">Edit</button><button class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.deleteuser(row.entity,rowRenderIndex)">Delete</button>',enableSorting: false },
		]
	}
   $scope.usersGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.usersGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;



	// 	//default set users
	getcount();
	get_data();

	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}

	//next page work
	$scope.nextPage = function(){
		if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_data();	
		}
	}
	
	//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_data();	
		}
	}
	function getcount(){
		//get total count of users
		users.getalluserscount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}

	function get_data(){
		users.getallusers($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.usersGrid.data = result;
			getcount();
		})
	}



// //edit page
	$scope.edituser =  function(user){
		console.log(user);
		 $state.go('admin.editUser', {'id': user._id});
	};

//Delete user
	$scope.deleteuser =  function(user, index){
		dialogs.openConfirmDialog('Are you sure want to delete user? :<b> '+user.profile_fields.first_name+'</b>','', function(yes,no){
	      if(no){
	        console.log("Delete user account");
	        return;
	      }else{
	      	users.deleteUser(user._id,function(err,result){
	      		if(err){	
					dialogs.openErrorDialog(err);return;
	      		}else{
	      			$scope.usersGrid.data.splice(index,1);	
	      		}
	      	})
	      }
	    })		
	}



// Search user by name or email
	$scope.searchUser = function(){

		delete $scope.config.searchEmail;
		delete $scope.config.searchName;

		if($scope.user.local.username != ''){
			$scope.config.searchName =  $scope.user.local.username;
		}
		else if($scope.user.local.email != ''){
			$scope.config.searchEmail = $scope.user.local.email;
			
		}
		users.getallusers($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.usersGrid.data = result;
		})

	}

	$scope.disableFilter = function(){
		delete $scope.config.searchEmail;
		delete $scope.config.searchName;
		$scope.user.local ={};
		get_data();
	}


}]);
