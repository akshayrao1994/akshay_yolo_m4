app.controller('attractionsCtrl', ['$scope','attractions','$state','categories','destinations','dialogs','uiGridConstants', function($scope,attractions,$state,categories,destinations,dialogs,uiGridConstants){


	$scope.ratings = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5];
	
	categories.getAllCategories(function(err,result){
		if(err){console.log("getAllCategories",err);}
		$scope.categories = result;
	})
	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})

	//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

	
 	//get fields for showing
	$scope.config = {
		'fields' : {'city' : 1,'category' : 1,'rating':1,'name':1,'id':1},
		'skip' : 0,
		'limit' : $scope.limit,
		'filter':{},
		sort :{"rating" :-1 }
	};
	$scope.attraction = {};

	//grid fields definition
	$scope.attractionsGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "Name" , field : "name"},
		{displayName : "Rating" , field : "rating"},
		{displayName : "City" , field : "city.city"},
		{displayName : "Category" , field : "category.title"},
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-show="row.entity.id" ng-click="grid.appScope.duplicateAttraction(row.entity)"  class="btn btn-xs btn-warning table_btns">Duplicate</button> <button ng-show="!row.entity.id" ng-click="grid.appScope.editAttraction(row.entity)"  class="btn btn-xs btn-warning table_btns">Edit</button><button ng-show="!row.entity.id" class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.deleteAttraction(row.entity,rowRenderIndex)">Delete</button>',enableSorting: false },
		]
	}
   $scope.attractionsGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.attractionsGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;
	
	// 	//default set attractions
	getcount();
	get_attractions();

	//duplicate attraction
	$scope.duplicateAttraction = function(attraction){
		attractions.duplicateAttraction(attraction._id,function(err,result){
			if(err){return console.log(err);}
			$state.go("admin.attractionsEdit",{id : result._id});
		})
	} 
	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}

// //edit page
	$scope.editAttraction =  function(attraction){
		 $state.go('admin.attractionsEdit', {'id': attraction._id});
	};

//next page work
	$scope.nextPage = function(){
	if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_attractions();	
		}
	}
//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_attractions();	
		}
	}
// Search attractions by name 
	$scope.searchAttraction = function(){
		if($scope.AttractionName != ''){
			$scope.config.name = $scope.AttractionName;
		}
		if($scope.filter){
			$scope.filter.rating || $scope.filter.rating==0 ? $scope.config.filter.rating = $scope.filter.rating : delete $scope.config.filter.rating;
			$scope.filter.category? $scope.config.filter.category = $scope.filter.category._id : delete $scope.config.filter.category;
			$scope.filter.city ? $scope.config.filter.city = $scope.filter.city._id : delete $scope.config.filter.city;
			
			if($scope.filter.type && $scope.filter.type == 'yolo'){
				$scope.config.filter.id = { $exists: false} ;
			}else if($scope.filter.type &&  $scope.filter.type == 'yelp'){
 				$scope.config.filter.id = { $exists: true} ;
			}else{
				delete $scope.config.filter.id; 
			}

			if( $scope.filter.openclosetime =='true'){
				$scope.config.filter.openclosetime= { $exists: true};
			}else if( $scope.filter.openclosetime =='false'){
				$scope.config.filter.openclosetime= { $exists: false};
			}else{
				delete $scope.config.filter.openclosetime;
			}

		}else{
			$scope.config.filter = {};
		}

		$scope.currentpage = 1;
		get_attractions();
	}

	$scope.disableFilter = function(){
		$scope.config.filter = {};
		delete $scope.config.name;
		$scope.filter={};
		get_attractions();
	}

//Delete user
	$scope.deleteAttraction =  function(attraction, index){

		dialogs.openConfirmDialog('Are you sure want to delete Attraction? :<b> '+attraction.name+'</b>','', function(yes,no){
	      if(no){
	        return;
	      }else{
	      	attractions.deleteAttraction(attraction._id ,function(err,result){
	      		if(err){
	      			return console.log('not able to delete',err);
	      			dialogs.openErrorDialog(err);
	      		}
				else{
					$scope.attractionsGrid.data.splice(index,1);
				}
	      	})
	      }
	    })		
	}


	function getcount(){
		attractions.getAttractionsCount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}
	

	function get_attractions(){
		attractions.getAllAttractions($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.attractionsGrid.data = result;
			getcount();
			$(window).resize()
		})
	}



}]);
