app.controller('attractionsCreateCtrl', ['$scope','attractions','categories','destinations','utils','$state','dialogs','fileupload','$filter', function($scope,attractions,categories,destinations,utils,$state,dialogs,fileupload,$filter){
	
	$scope.filers = {};
	$scope.timing = [];
	$scope.ratings = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5];
	$scope.allDays = {1:"sunday",2:"monday",3:"tuesday",4:"wednesday",5:"thursday",6:"friday",7:"saturday"};

    $scope.clickme = function() {
        $scope.timing.push({});
    }

	$scope.removeopenclosetime = function(index){
		$scope.timing.splice(index,1);
	}    
	
	categories.getAllCategories(function(err,result){
		if(err){console.log("getAllCategories",err);}
		$scope.categories = result;
	})
	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})
	utils.getAllLanguages(function(err,result){
		if(err){console.log("getAllLanguages",err);}
		$scope.languages = result;		
	})
	$scope.language_change = function(){
		$scope.attraction.language = $scope.languages[$scope.attraction.language_code];
	}

	$scope.alwaysopen = function(){
		//console.log("test"+$scope.attraction.is_always_open);
		//console.log(document.getElementById('is_always_open').checked);
		if($scope.attraction.is_always_open == 'true'){
			//console.log('what?');
			if($scope.timing.length > 0){
				dialogs.openConfirmDialog('Warning Message','Your custom time settings will be erased. Do you want to continue?', function(yes,no){
	      			if(no){
	        			$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'false' : $scope.attraction.is_always_open = 'true';
	        		return;
	      			}else{
	      				$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'true' : $scope.attraction.is_always_open = 'false';
	      			}
	    		})				
			}
		}
	}


	$scope.createAttraction = function(){
		$scope.count = 1;
		var days1 = [];
		if($scope.CreateAttractionForm.$valid){
			$scope.attraction.openclosetime = [];
			if($scope.attraction.is_always_open == 'true'){
				//console.log('$scope.attraction.is_always_open',$scope.attraction.is_always_open);
				$scope.attraction.is_always_open = true;
				delete $scope.timing;
			}
			if($scope.timing){
			for(var i=0;i<$scope.timing.length;i++){
				days1[i] = {};
				days1[i].day = $scope.timing[i].day;
				days1[i].opentime = $filter('date')($scope.timing[i].opentime, 'HH:mm');
				days1[i].closetime = $filter('date')($scope.timing[i].closetime, 'HH:mm');
			}
			//console.log('timing',$scope.timing);
		}
			$scope.attraction.openclosetime = days1;
			$scope.error = false;
			fileupload.uploadfile($scope.temp_file,'attractions',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else{
					$scope.attraction.image_url = result;
					attractions.createAttraction($scope.attraction ,function(err,result){
						if(err){
							if(err.message){
								return $scope.error = err.message;
							}else{
								return dialogs.openErrorDialog(err);
							}
						}else{
							dialogs.openMessageDialog("Attraction Created");
							$state.go("admin.attractions");
						}
					})
				}
			});
		}else{
			$scope.submitted = true;
		}
	}


}]);
