app.controller('attractionsEditCtrl', ['$scope', 'attractions', 'categories', 'destinations', 'utils', '$state', 'dialogs', 'fileupload', '$filter', function($scope, attractions, categories, destinations, utils, $state, dialogs, fileupload, $filter) {
    $scope.filers = {};
    $scope.ratings = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];
    $scope.allDays = { 1: "sunday", 2: "monday", 3: "tuesday", 4: "wednesday", 5: "thursday", 6: "friday", 7: "saturday" };
    $scope.timing = [];

    $scope.clickme = function() {
        $scope.timing.push({});
    }

	$scope.removeopenclosetime = function(index){
		$scope.timing.splice(index,1);
	}

    $scope.cancel = function(){
        $state.go('admin.attractions');
    }

    categories.getAllCategories(function(err, result) {
        if (err) { console.log(err); }
        $scope.categories = result;
    })
    destinations.getAllDestinations({}, function(err, result) {
        if (err) { console.log("getAllDestinations", err); }
        $scope.destinations = result;
        //console.log('destinations',result);
    })
    utils.getAllLanguages(function(err, result) {
        if (err) { console.log("getAllLanguages", err); }
        $scope.languages = result;
    })

    attractions.getAttraction($state.params.id, function(err, result) {
        if (err) {
            dialogs.openErrorDialog("Wrong Attraction Id");
            $state.go("admin.attractions");
        } else {
            // console.log(result);
            $scope.attraction = result;
            for (var a = 0; a < result.openclosetime.length; a++) {
                // // var opentime =new Date(result.openclosetime[a].open.time).toLocaleTimeString();
                // var open =$filter('date')(result.openclosetime[a].open.time, 'HH:mm');
                // console.log("sdsds"+open);
                var opening = new Date();
                opening.setHours(result.openclosetime[a].open.time.split(':')[0]);
                opening.setMinutes(result.openclosetime[a].open.time.split(':')[1]);
                var opendate = $filter('date')(opening, 'yyyy-MM-dd hh:mm:a');

                var closing = new Date();
                closing.setHours(result.openclosetime[a].close.time.split(':')[0]);
                closing.setMinutes(result.openclosetime[a].close.time.split(':')[1]);
                var closedate = $filter('date')(closing, 'yyyy-MM-dd hh:mm:a');
                // console.log('hello', typeof opening);
                $scope.timing[a] = {};
                $scope.timing[a].opentiming = new Date(opendate);
                $scope.timing[a].day = result.openclosetime[a].open.day;
                $scope.timing[a].closetiming = new Date(closedate);
            }
            //console.log('openclosetime', result.openclosetime);
        }
        if($scope.timing.length > 0){
        	$scope.attraction.is_always_open = 'false';
        }
        else{
        	$scope.attraction.is_always_open = 'true';
        }
    })

    $scope.language_change = function() {
        $scope.attraction.language = $scope.languages[$scope.attraction.language_code];
    }

/*	$scope.alwaysopencheckbox = function(){
		dialogs.openConfirmDialog('Are you sure you want to revert this checkbox? All your timings Data will be deleted.','', function(yes,no){
	      if(no){
	        //$scope.attraction.is_always_open = false;
	        $scope.attraction.is_always_open ? $scope.attraction.is_always_open = false : $scope.attraction.is_always_open = true;
	        return;
	      }else{
	      	//$scope.attraction.is_always_open = true;
	      	$scope.attraction.is_always_open ? $scope.attraction.is_always_open = true : $scope.attraction.is_always_open = false;
	      }
	    })
	}*/

	$scope.alwaysopen = function(){
		//console.log("test"+$scope.attraction.is_always_open);
		//console.log(document.getElementById('is_always_open').checked);
		if($scope.attraction.is_always_open == 'true'){
			//console.log('what?');
			if($scope.timing.length > 0){
				dialogs.openConfirmDialog('Warning Message','Your custom time settings will be erased. Do you want to continue?', function(yes,no){
	      			if(no){
	        			$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'false' : $scope.attraction.is_always_open = 'true';
	        		return;
	      			}else{
	      				$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'true' : $scope.attraction.is_always_open = 'false';
	      			}
	    		})				
			}
		}
	}

    $scope.updateAttraction = function() {
        var days1 = [];
        //console.log('data here', $scope.attraction);
        //console.log('data here is ', $scope.timing);
		if($scope.attraction.is_always_open == 'true'){
				//console.log('$scope.attraction.is_always_open',$scope.attraction.is_always_open);
			$scope.attraction.is_always_open = true;
			delete $scope.timing;
		}
        $scope.attraction.openclosetime = [];
        if($scope.timing){
        for (var i = 0; i < $scope.timing.length; i++) {
            days1[i] = {};
            days1[i].day = $scope.timing[i].day;
            days1[i].opentiming = $filter('date')($scope.timing[i].opentiming, 'HH:mm');
            days1[i].closetiming = $filter('date')($scope.timing[i].closetiming, 'HH:mm');
        }
    }
        $scope.attraction.openclosetime = days1;
        if ($scope.EditAttractionForm.$valid) {
            if ($scope.temp_file) {
                $scope.error = false;
                fileupload.uploadfile($scope.temp_file, 'attractions', function(err, result) {
                    if (err) { console.log("file not uploaded", err); } else {
                        $scope.attraction.image_url = result;
                        attractions.updateAttraction($scope.attraction, function(err, result) {
                            if (err) {
                                if (err.message) {
                                    return $scope.error = err.message;
                                } else {
                                    return dialogs.openErrorDialog(err);
                                }
                            } else {
                                dialogs.openMessageDialog("Attraction Updated");
                                $state.go("admin.attractions");
                            }
                        })
                    }
                });
            } else {
                attractions.updateAttraction($scope.attraction, function(err, result) {
                    if (err) {
                        if (err.message) {
                            return $scope.error = err.message;
                        } else {
                            return dialogs.openErrorDialog(err);
                        }
                    } else {
                        dialogs.openMessageDialog("Attraction Updated");
                        $state.go("admin.attractions");
                    }
                })
            }
        } else {
            console.log($scope.EditAttractionForm);
            $scope.submitted = true;
        }
    }

}]);
