app.controller('destinationsCreateCtrl', ['$scope','destinations','$state','dialogs','fileupload','dataService', function($scope,destinations,$state,dialogs,fileupload,dataService){
	destinations.getCountries(function(err,result){
		if(err){console.log(err);}
		else{
			$scope.countries = result;
		}
	})
	$scope.createDestination = function(){
		var city = $scope.destination.city+" "+$scope.destination.country;
		dataService.getCoordinatesFromAddress(city, function(err, data) {
            console.log('data',data);
            $scope.destination.location ={};
            $scope.destination.location.lng = data.results[0].geometry.location.lng;
            $scope.destination.location.lat = data.results[0].geometry.location.lat;
        });

		if($scope.createDestinationForm.$valid){
			fileupload.uploadfile($scope.temp_file,'destinations',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else{
					$scope.destination.image = result;
					destinations.destinationCreate($scope.destination,function(err,result){
						if(err){
							dialogs.openErrorDialog("Destination Not Created");
							console.log(err);
						}
						else{
							dialogs.openMessageDialog("Destination Created");
							$state.go("admin.destinations");
						}
					})
				}

			})
		}else{
			$scope.error = "Form validation Failed";
		}


	}
}]);