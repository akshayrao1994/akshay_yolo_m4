app.controller('destinationsEditCtrl', ['$scope','destinations','$state','dialogs','fileupload', function($scope,destinations,$state,dialogs,fileupload){
	destinations.getCountries(function(err,result){
		if(err){console.log(err);}
		else{
			$scope.countries = result;
		}
	})

	destinations.getDestination($state.params.id, function(err,result){
		if(err){console.log(err);}
		else{
			$scope.destination = result;
		}
	})
	$scope.editDestination = function(){
		if($scope.temp_file){
			fileupload.uploadfile($scope.temp_file,'destinations',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else{
					$scope.destination.image = result;
					destinations.destinationUpdate($scope.destination,function(err,result){
						if(err){
							dialogs.openErrorDialog("Destination Not Updated");
							console.log(err);
						}
						else{
							dialogs.openMessageDialog("Destination Updated");
							$state.go("admin.destinations");
						}
					})
				}

			})
		}else{
			destinations.destinationUpdate($scope.destination,function(err,result){
				if(err){console.log(err);dialogs.openErrorDialog("Destination Not Updated ");}
				else{
					dialogs.openMessageDialog("Destination Updated");
					$state.go("admin.destinations");
				}
			})	
		}
	}
}]);
