app.controller('destinationsCtrl', ['$scope','$state','dialogs','$rootScope','uiGridConstants','destinations', function($scope,$state,dialogs,$rootScope,uiGridConstants,destinations){
	

//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

// 	//get fields for showing
	$scope.config = {
	'fields' : {'city' : 1,'country' : 1},
	'filter':{},
	'skip' : 0,
	'limit' : $scope.limit,
	}
	$scope.destinations = {};
   
	//grid fields definition
	$scope.DestinationsGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "City" , field : "city"},
		{displayName : "Country" , field : "country"},
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-click="grid.appScope.editDestination(row.entity)" class="btn btn-xs btn-warning table_btns">Edit</button><button class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.DeleteDestination(row.entity,rowRenderIndex)">Delete</button>',enableSorting: false },
		]
	}
   $scope.DestinationsGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.DestinationsGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;



	// 	//default set attractions
	getcount();
	get_data();

	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}

	//next page work
	$scope.nextPage = function(){
		if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_data();	
		}
	}
	
	//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_data();	
		}
	}
	function getcount(){
		//get total count of users
		destinations.getDestinationsCount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}

	function get_data(){
		destinations.getAllDestinations($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.DestinationsGrid.data = result;
			getcount();
		})
	}



// //edit page
	$scope.editDestination =  function(destination){
		 $state.go('admin.destinationsEdit', {'id': destination._id});
	};

//Delete user
	$scope.DeleteDestination =  function(destination, index){
		dialogs.openConfirmDialog('Are you sure want to delete Destination? :<b> '+destination.city+'</b>','', function(yes,no){
	      if(no){
	        console.log("Delete user account");
	        return;
	      }else{
	      	destinations.destinationDelete(destination._id ,function(err,result){
	      		if(err){	
					dialogs.openErrorDialog(err);return;
	      		}else{
	      			$scope.DestinationsGrid.data.splice(index,1);	
	      		}
	      	})
	      }
	    })		
	}



// Search destination
	$scope.searchDestinations = function(){
		get_data();
	}
	
	$scope.disableFilter = function(){
		delete $scope.config.city;
		get_data();
	}

}]);
