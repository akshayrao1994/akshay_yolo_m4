app.controller('singleAttractionCtrl', ['$scope', 'attractions', '$window', '$location', 'journals', 'users', 'tags', 'comments', '$rootScope', 'localStorageService', 'dialogs', '$state', function($scope, attractions, $window, $location, journals, users, tags, comments, $rootScope, localStorageService, dialogs, $state) {
$scope.allDays = {1:"sunday",2:"monday",3:"tuesday",4:"wednesday",5:"thursday",6:"friday",7:"saturday"};

    $scope.rating1 = 3;
    $scope.isReadonly = true;
    $scope.rating=1;

$scope.stars=[1,2,3,4,5];


    $scope.bookmarkAttraction = function(attraction) {
        attraction.bookmarkuser = $rootScope.loggedInUser;
        attractions.bookmarkAttraction(attraction, function(err, result) {
            if (err) {
                return console.log('not able to bookmark', err);
                dialogs.openErrorDialog(err);
            } else {
                attractiondata();
                dialogs.openMessageDialog("Attraction Bookmarked");
                // console.log('result is',result);
                // $rootScope.loggedInUser = result;
                //$scope.attractionsGrid.data.splice(index,1);
            }
        })
    }

$scope.gotoAttraction = function(_id){
    $state.go('index.singleAttraction',{'id': _id})
}

$scope.changeimage = function(url){
    console.log('url',url);
    $scope.img = url;
}

$scope.config = {
    'fields' : {},
    'filter':{},
    'sort' :{rating:-1},
        'skip' : 0,
        'limit' : 3,
};
    $scope.attractiondata = function() {
        attractions.getAttraction($state.params.id, function(err, result) {
            if (err) { dialogs.openErrorDialog(err);
                return } else {
                // console.log('result123',result);
                $scope.attraction = result;
                $scope.img = result.image_url[0];
                // console.log($scope.attraction);
                // $scope.rating = result.rating;
                // $scope.rating1 = result.rating * 20;
                $scope.config.filter.city = result.city._id;
                $scope.config.filter._id = {$ne:result._id};
                $scope.allAttractions();
            }
        })
    }


$scope.allAttractions = function(){
    attractions.getAllAttractions($scope.config,function(err,result){
        if(err) { dialogs.openErrorDialog(err);
            return
        }
        else{
            // console.log('result',result);
            $scope.allattractions = result;
            }
    })
}

$scope.attractiondata();

$scope.editattraction = function(){
    $state.go('index.editAttraction', {'id': $state.params.id});
}





    // $scope.attraction.rating = 1;
    $scope.rateFunction = function(rating) {
        console.log('Rating selected: ' + rating);
        var data = {};
        data._id = $state.params.id;
        data.user_id = $rootScope.loggedInUser._id;
        data.rating = rating;
        attractions.updateReviews(data,function(err,result){
            if(err) {return err};
            
                console.log('result is',result);
            
        })
    };



}])
