app.controller('editJournalCtrl', ['$scope', 'users', 'journals', 'utils', 'destinations', 'dialogs', 'tags', '$state', '$rootScope', 'localStorageService', 'fileupload', function($scope, users, journals, utils, destinations, dialogs, tags, $state, $rootScope, localStorageService, fileupload) {
    $scope.journal = {};

    $scope.config = {
        'fields': { 'name': 1 },
        'filter': {},
        'sort': { 'name': 1 },
    };

    $scope.getTags = function(query) {
        var regex = new RegExp(query, 'gi');
        return _.filter($scope.tag, function(obj) {
            return obj.name.match(regex); });
    }

    $scope.languages = { "zn": "Chinese", "en": "English" };

    //get journal for edit
    journals.getJournal($state.params.id, function(err, result) {
        if (err) { dialogs.openErrorDialog(err);
            return } else {
            //console.log('result is', result);
            $scope.journal = result;
        }
    })

    tags.getallTags($scope.config, function(err, result) {
        if (err) { console.log("getalltags", err); }
        $scope.tag = result;
        return;
    })


    destinations.getAllDestinations({}, function(err, result) {
        if (err) { console.log("getAllDestinations", err); }
        $scope.destinations = result;
    })

    $scope.cancel = function() {
        $state.go('index.singleJournal', {'id': $state.params.id});
    }

    $scope.language_change = function() {
        $scope.journal.language = $scope.languages[$scope.journal.language_code];
    }

    $scope.UpdateJournal = function() {
        $scope.error = null;
        if ($scope.createForm.$valid) {
            if ($scope.temp_file) {
                fileupload.uploadfile($scope.temp_file, 'journals', function(err, result) {
                    if (err) { console.log("file not uploaded", err); } else {
                        $scope.journal.image_url = result;
                        journals.updatejournal($state.params.id, $scope.journal, function(err, result) {
                            if (err) {
                                if (err.message) {
                                    $scope.error = err.message;
                                } else {
                                    dialogs.openErrorDialog(err);
                                }
                                return;
                            } else {
                                dialogs.openMessageDialog('Journal Updated Successfully');
                                $state.go('index.singleJournal', {'id': $state.params.id});
                            }

                        })
                    }

                })
            } else {
                journals.updatejournal($state.params.id, $scope.journal, function(err, result) {
                    if (err) {
                        if (err.message) {
                            $scope.error = err.message;
                        } else {
                            dialogs.openErrorDialog(err);
                        }
                        return;
                    } else {
                        dialogs.openMessageDialog('Journal Updated Successfully');
                        $state.go('index.singleJournal', {'id': $state.params.id});
                    }

                })
            }
        } else {
            $scope.error = "Form Validation Failing.."
        }
    }

}]);
