app.controller('singleJournalCtrl', ['$scope','$window','$location','journals','users','tags','comments','$rootScope','localStorageService','dialogs','$state', function($scope,$window,$location,journals,users,tags,comments,$rootScope,localStorageService,dialogs,$state){
var journalData; 
var totalcomment;
var data = {};
$scope.tag_name=[];
$scope.comment_user = [];

//$scope.totalcomments=[];
$scope.currentUrl = $location.absUrl();
//console.log('asvh',$scope.currentUrl);

// $scope.limit =10;
	$scope.config = {
	'fields' : {},
	'filter':{journal: $state.params.id},
		'sort' :{'created_on' :-1},
		'skip' : 0,
		// 'limit' : $scope.limit,
        'limit' : 0,
	};

$scope.user = $rootScope.loggedInUser;
//console.log('sagyh',$rootScope);

$scope.journaldata = function(){
    journals.getJournal($state.params.id,function(err,result){
        if(err){dialogs.openErrorDialog(err);return}
        else{
            //result.status = String(result.status);
            //result.isadmin = String(result.isadmin);
            $scope.journal = result;
            $scope.journal.shareImageUrl = $rootScope.base_path+ result.image_url;
            journalData = result;
            // console.log('fastygh',result);
            users.getUser(journalData.created_by,function(err,result){
        	if(err){dialogs.openErrorDialog(err);return}
        	else{
                //console.log('result here is',result);
        		$scope.journal.created_user = result.local.email;
                $scope.journal.image = result.image;
        		}
    		})
/*    		for(var i=0;i<journalData.tags.length;i++){
            	tags.getTag(journalData.tags[i]._id,function(err,result){
        		if(err){dialogs.openErrorDialog(err);return}
        		else{
        			$scope.tag_name.push(result.name);
        		}
    		})
    		}*/
    		comments.getallComments($scope.config,function(err,result){
    			if(err){$scope.err = err; console.log(err); return;}
    			$scope.cmmnts = result;
    			// console.log('result',result);
    		})
        }
    })
}

$scope.journaldata();


    $scope.submitcomment = function(){
    	data.description = $scope.newcomment;
    	data.journal = $state.params.id;
        //console.log('data is',data);
    	comments.createComment(data,function(err,result){
    		if(err){$scope.err = err; console.log(err); return;}
    		//console.log('new comment updated');
    		journaldata();
            dialogs.openMessageDialog("Thank You! Your Comment has been sent for approval");
    		$scope.newcomment ="";

    	})
    }

$scope.like = function(){
    var data = {};
    data.likeBy = $rootScope.loggedInUser._id;
    journals.likejournal($state.params.id,data,function(err,result){
        if(err) {throw err;}
        //console.log('result',result);
        $scope.journal.likeBy = result.likeBy;

    })
}

//console.log('id of journal is ',$state.params.id);

$scope.editjournal = function(){
    $state.go('index.editJournal', {'id': $state.params.id});
}

/*$scope.unlike = function(){
    var data = {};
    data.unlikeBy = $rootScope.loggedInUser._id;
    console.log('gfchvjb',data);
    journals.unlikejournal($state.params.id,data,function(err,result){
        if(err) {throw err;}

        console.log('result is',result);
        $scope.journal.likeBy = result.likeBy;

    })
}*/



}])