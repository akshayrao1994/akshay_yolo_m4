app.controller('createJournalCtrl', ['$scope','$rootScope','attractions','categories','destinations','journals','utils','tags','$state','dialogs','fileupload', function($scope,$rootScope,attractions,categories,destinations,journals,utils,tags,$state,dialogs,fileupload){
	$scope.publish = [true,false];
var taghere;
	$scope.config = {
	'fields' : {'name' : 1},
	'filter':{},
	'sort' :{'name' :1},
	};
	// utils.getAllLanguages(function(err,result){
	// 	if(err){console.log("getAllLanguages",err);}
	// 	$scope.languages = result;		
	// })0
	!$rootScope.editProfileTab ? $rootScope.editProfileTab = "journal" : null;
	$scope.switchTab = function(name){
		$rootScope.editProfileTab = name;
	}

    $scope.languages = {"zn":"Chinese","en":"English"};

	$scope.language_change = function(){
		$scope.journal.language = $scope.languages[$scope.journal.language_code];
	}

	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})

	tags.getallTags($scope.config, function(err,result){
		if(err){console.log("getalltags",err);}
		$scope.tag = result;
		taghere = result;
/*		$scope.tags =[{name:"1111"},{name :"22222"}];*/
		return;
	})

	$scope.getTags = function(query){
     var regex = new RegExp(query, 'gi');
     return _.filter($scope.tag, function(obj){ return obj.name.match(regex);});
    }

	$scope.cancel = function(){
		$state.go('index.profile');
	}

	$scope.createJournal = function(){		
		if($scope.journalForm.$valid)
		{
			$scope.error = false;
			fileupload.uploadfile($scope.temp_file,'journals',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else
				{
					$scope.journal.image_url = result;
					journals.createJournal($scope.journal ,function(err,result){
						if(err)
						{
							if(err.message)
							{
								return $scope.error = err.message;
							}
							else
							{
								return dialogs.openErrorDialog(err);
							}
						}
						else
						{
							dialogs.openMessageDialog("Thank You! Your Journal has been sent to the admin for verification. Your journal will be live immediately after the admin approves it :) ");
							$state.go("index.profile");
						}
					})
				}
			});
		}
		else
		{
			$scope.submitted = true;
		}
	}

}]);