app.controller('forgotCtrl', ['$scope','users','dialogs','$state', function($scope,users,dialogs,$state){
		$scope.initialize = function(){
			$scope.TitleField = "Enter Your Password";
			$scope.errorClass = "";
		} 
		$scope.initialize();



	if($state.params.token && $state.params.email){
		$scope.setnewpassword = true;
			
			var config = {
			'filter' : {
				'local.email' : $state.params.email,
				'verification_code' : $state.params.token,
			}
		};
		users.getallusers(config,function(err,result){
			if(result.length == 0){
				dialogs.openErrorDialog("Wrong Email /Token");
				$state.go("index.home");
			}
		
		})

		$scope.setPassword = function(){
			$scope.initialize();
			$scope.setNewPassword.password.$touched =true;
			$scope.setNewPassword.cpassword.$touched =true;
			
			if($scope.setNewPassword.$valid && $scope.user.password  == $scope.user.cpassword){
				users.saveNewPassword( $state.params.email, $state.params.token,$scope.user.password,function(err,result){
					if(err) {console.log("Server Error ",err);return;}
					dialogs.openMessageDialog("<p>Your Password is Updated</p>");
					$state.go('index.login');
				})
			}else{
				$scope.errorClass = "error";
				$scope.TitleField = "Password not match";
			}
		}

	}else{

	 	$scope.forgot = function(){
	 		$scope.initialize();
			$scope.forgotPassword.email.$touched =true;

	 		if($scope.forgotPassword.$valid){
				users.forgotPassword($scope.user.email,function(err,result){
					if(err){
						if(err.message){
							$scope.errorClass = "error";
							$scope.TitleField = err.message;
						}else{
							console.log('Server Error',err);
						}
						return;
					}

					dialogs.openMessageDialog("<p>Please check your email and click forgot Password in the message we just sent to "+$scope.user.email+"</p>");
				}) 		 			
	 		}

	 	}
	}



	




}]);