app.controller('profileCtrl', ['$scope','users','$rootScope','localStorageService','dialogs','$state', function($scope,users,$rootScope,localStorageService,dialogs,$state){
	$scope.user = localStorageService.get('yolo_itinerary_client');
	$scope.changeState = function(name){
		$rootScope.editProfileTab = name;
	}
}])