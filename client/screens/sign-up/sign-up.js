app.controller('signupCtrl', ['$scope','users','dialogs', function($scope,users,dialogs){
	$scope.user = {};
	
	$scope.initilizeVariables = function(){
		$scope.titles = {};
		$scope.titles.email      = "Enter Your Email";
		$scope.titles.first_name = "Enter Your Name";
		$scope.titles.password   = "Enter Your Password";
		$scope.titles.cpassword  = "Cofirm your Password";

		$scope.classes = {};
		$scope.classes.email      = "";
		$scope.classes.first_name = "";
		$scope.classes.password   = "";
		$scope.classes.cpassword  = "";	
	}
	//initialize variables 
	$scope.initilizeVariables();
	
	$scope.signUp = function(){
		$scope.initilizeVariables();

		if($scope.createForm.$valid && $scope.user.password == $scope.user.confirmPassword){		
			console.log("uuu",$scope.user);
			users.signup($scope.user , function(err,result){
				if(err){
					//if error send by me
					if(err.message){
						$scope.titles.email  = err.message;
						$scope.classes.email = "error";
					}else{
						console.log("Server Error :",err);
					}
					return;
				}else{
					dialogs.openMessageDialog("<p>Thanks For Signing Up!</p><p>Please check your email and click Activate Account in the message we just sent to "+$scope.user.email+"</p>");
				}
			})
		}else{
			//set touched 
			$scope.createForm.email.$touched =true;
			$scope.createForm.first_name.$touched =true;
			$scope.createForm.password.$touched =true;
			$scope.createForm.cpassword.$touched =true;

			//password filled but not matched
			if( $scope.user.password !== $scope.user.confirmPassword){
				$scope.classes.password = "error";
				$scope.classes.cpassword = "error";

				$scope.titles.password  = "Password doesn\'t match";
				$scope.titles.cpassword = "Password doesn\'t match";
			}
		}
	}
}]);