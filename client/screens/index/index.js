app.controller('indexCtrl', ['$scope','$rootScope','users','localStorageService','$location','$state', function($scope,$rootScope,users,localStorageService,$location,$state){
	 // $rootScope.base_path = "http://localhost:8080";
	// $rootScope.base_path = "http://testsite.com:8080";
	// $rootScope.base_path = "http://139.162.5.142:8080";
	$rootScope.base_path = $location.protocol() + '://' + $location.host()+":"+$location.port();
	//check if login and set rootscope and  storage accordingly
	users.islogin();
	
	$scope.changeState = function(name){
		$rootScope.editProfileTab = name;
		console.log(name);
	}

	$scope.logout = function(){
		users.logout(function(err,result){
			if(result){
				$rootScope.loggedInUser = null;
				localStorageService.remove('yolo_itinerary_client');
					$state.go("index.home")			
				console.log('logout successfully');
			}
		});
	}

}]);