app.controller('itinerarysEditCtrl', ['$scope','$rootScope','$state','itinerarys','destinations','attractions','categories','dialogs','$filter','$sce','$timeout', function($scope,$rootScope,$state,itinerarys,destinations,attractions,categories,dialogs,$filter,$sce,$timeout){
	//for time field to show am and pm
	$scope.ctrl = {};
   $timeout(function() {
        $scope.ctrl.ready = true;
      }, 1000);

	$scope.hstep = 1;
	$scope.mstep = 15;
	$scope.mytime = new Date();
	$scope.updating_itinerary_days =false;
	$scope.showSidebar = false;
	$scope.searchCategory = 'search';
	
	$scope.mapday = "1";
  	$scope.showingMap = false;

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }

$scope.slideron = 1;

  $scope.move = function(move){
		if($scope.slideron > 1 && move == "-1"){
			$scope.slideron = $scope.slideron+ Number(move);
		}
		if($scope.slideron < $scope.total_days && move == "+1"){
			$scope.slideron = $scope.slideron+ Number(move);
		}
  }
  $scope.toggleSidebar = function(){
  	$scope.showSidebar = !$scope.showSidebar; 
  }
  $scope.searchCategorySwitch = function(show){
  	$scope.searchCategory = show;
  }
  $scope.toggleTravelMode = function(mode){
  	if($scope.currentMode == mode){
  		$scope.currentMode = null
  	}else{
  	$scope.currentMode = mode;
  		
  	}
  }
	//get itinerary from machine_name and id
	itinerarys.getItinerary($state.params.machine_name,$state.params.id,function(err,result){

		if(err){console.log("err is getting itinerary",err);}
		console.log(result);
		if(result == ""){
			alert("wrong id");
		}
		$scope.itinerary = result;
		$scope.itinerary_copy = angular.copy(result);

		//create days json 
		if(!result.days || result.days && result.days.length == 0 ){
			var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
			var days = diff/(24*60*60*1000);
			$scope.total_days = days;

			$scope.itinerary.days={};

			for(i=0;i<days;i++){
				$scope.itinerary.days[i+1]=[];
			}
		}else{
			console.log($scope.itinerary.days);
			$scope.total_days = Object.keys($scope.itinerary.days).length;
		}

	})
	$scope.stayDays = function(destination){
		var start_date =  new Date(destination.start_date);
		var end_date =  new Date(destination.end_date);
		var diff = end_date.getTime() -  start_date.getTime(); 
		return (diff /1000/3600/24) + " Days"; 


	}
	$scope.showMap = function(){
		var map_addr = "http://maps.google.com/maps?saddr=";
		if($scope.itinerary.days[$scope.mapday]){
			$scope.itinerary.days[$scope.mapday].forEach(function(day,index){
				if(index == 0){
					map_addr = map_addr + day.address;					
				}else if(index == 1 ){
					map_addr = map_addr+ "&daddr=" + day.address;					
				}else{
					map_addr = map_addr + " to:"  + day.address;					
				}
			})
			map_addr = map_addr+ "&output=embed";
			$scope.mapSrc = map_addr;
			$scope.showingMap = true;
		}
	
	}
	$scope.hideMap =function(){
		$scope.showingMap = false;

	}
	//All Sidebar Work First

    //get all destinations for filters
	destinations.getAllDestinations({},function(err,result){
		if(err){return console.log(err);}
		$scope.destinations = result;
		$scope.trip_to_destinations = angular.copy(result);
	})
    //get all categories for filters
	categories.getAllCategories(function(err,result){
		if(err){return console.log(err);}
		$scope.categories = result;
	})

	$scope.search = {};
	$scope.search.filter = {};
	$scope.search.limit = 10;

	$scope.searchAttractions= function(){
		$scope.search.filter.city == '' ? delete $scope.search.filter.city : null; 
		$scope.search.filter.category == '' ? delete $scope.search.filter.category : null; 
		$scope.search.name == '' ? delete $scope.search.name : null; 
		
		attractions.getAllAttractions($scope.search,function(err,result){
			if(err){return console.log(err);}
			$scope.attractions = result;
		})
	}

	//call for first time attractions loading
	$scope.searchAttractions();

	//Itinierary
	//timepicker related functions
	$scope.showTimepicker = function(type,index){
		$scope.timepicker_view = type+index;
	}

	$scope.hideTimepicker =function(){
		$scope.timepicker_view = null;	
		$scope.itinerary = $scope.itinerary_copy;
	}
	//remove attraction
	$scope.removeAttraction = function(day,index){
		$scope.itinerary.days[day].splice(index,1);
		$scope.calulateTime(day);
	}

	//calculate time of day of attractions
	$scope.calulateTime = function(day){
		$scope.updating_itinerary_days = true;

		var itinerary =  angular.copy($scope.itinerary);
		
		for(i=0;i<itinerary.days[day].length;i++){
			itinerary.days[day][i].start_time = $filter('date')(new Date(itinerary.days[day][i].start_time), 'HH:mm');
			itinerary.days[day][i].stay_time = $filter('date')(new Date(itinerary.days[day][i].stay_time), 'HH:mm');
		}
		itinerarys.getDayDistanceTime(itinerary.days[day],function(err,result){
			if(err){
				$scope.itinerary = angular.copy($scope.itinerary_copy);
				return dialogs.openErrorDialog(err.message); 
			}

				for(i=0;i<result.length;i++){
					var start_time = result[i].start_time.split(":");
					result[i].start_time = new Date("Sat May 15 2016 08:00:00")
					result[i].start_time.setHours(start_time[0]);
					result[i].start_time.setMinutes(start_time[1]);

					var stay_time = result[i].stay_time.split(":");
					console.log("stay time",stay_time);
					result[i].stay_time = new Date("Sat May 15 2016 08:00:00");
					result[i].stay_time.setHours(stay_time[0]);
					result[i].stay_time.setMinutes(stay_time[1]);
				}
				$scope.itinerary.days[day] = result;
				$scope.updating_itinerary_days = false;
				$scope.itinerary_copy = angular.copy($scope.itinerary);
		})
	}

	//called on every drop
    $scope.dropCallback = function(event, index, item, external, type, allowedType,day) {
    	//for sidebar  no drop should work
    	if(day == 'search'){
    		return;
    	}
  		var newitem ={};
		newitem.name = item.name;
		newitem.id = item._id;
		newitem.image_url = item.image_url;
		newitem.longitude = item.longitude;
		newitem.latitude = item.latitude;
		newitem.address = item.address;
		newitem.type = "attraction";
		return newitem;
    };

    //show the different modes and work on chnage 
    $scope.showDiffMode = function(key , index){
    	itinerarys.getAllModes($scope.itinerary.days[key][index],function(err,result){
    		if(err){return console.log("error in getting modes ",err );}
    		$scope.travelModes = result;

    		dialogs.openTravelModeDialog($scope,function(err,result){
    			if(err){return;}
				$scope.itinerary.days[key][index].next_travel_mode = result; 
    			$scope.calulateTime(key);
    		});
    	})
    }
    $scope.saveItinerary =function(mode){
    	if(mode == 'draft'){
    		itinerarys.updateItinerary($state.params.id,{published:false,days:$scope.itinerary.days},function(err,result){
    			if(err){return console.log(err);}

    			dialogs.openMessageDialog("Itinerary Saved in Draft Mode");
    		})
    	}
    	if(mode == 'finish'){
    		itinerarys.updateItinerary($state.params.id,{published:true , days:$scope.itinerary.days},function(err,result){
    			if(err){return console.log(err);}
    			dialogs.openMessageDialog("Itinerary Saved in Publish Mode");

    		})
    	}
    }

    $scope.deleteItinerary = function(){
    	itinerarys.deleteItinerary($state.params.id,function(err,result){
    		if(err){return console.log("err",err);}
				dialogs.openMessageDialog("Itinerary Deleted")
    	})
    }
    $scope.dayend=function(attraction){
    	if(attraction.start_time){
    		var start_time = new Date(attraction.start_time).getTime();
			var stay_hours = new Date(attraction.stay_time).getHours();
			var stay_minutes =new Date(attraction.stay_time).getMinutes();
			stay_minutes = stay_hours * 60 + stay_minutes;
			stay_minutes = stay_minutes * 60 *1000;
			return new Date(start_time + stay_minutes);

    	}

    }

}]);