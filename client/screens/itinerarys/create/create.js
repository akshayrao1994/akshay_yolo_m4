app.controller('itinerarysCreateCtrl', ['$scope','$rootScope','$state','itinerarys','destinations','attractions','categories','dialogs','utils', function($scope,$rootScope,$state,itinerarys,destinations,attractions,categories,dialogs,utils){
	$scope.popup = {};
	$scope.popup ={};


	  // Disable weekend selection
	  function disabled(data) {
	    return false;
	  }
	$scope.open = function(index, num) {
		$scope.popup[index] = [];
		$scope.popup[index][num] = true;
	};

	  $scope.dateOptions = {
	    showWeeks: false,
	    dateDisabled: disabled,
	    formatYear: 'yy',
	    maxDate: new Date(2020, 5, 22),
	    minDate: new Date(),
	    startingDay: 1
	  };

	  $scope.setDate = function(year, month, day) {
	    $scope.dt = new Date(year, month, day);
	  };
		
	  destinations.getAllDestinations({},function(err,result){
	  		$scope.destinations = result;
	  })
	  
	  $scope.createItinerary = function(){
	  	itinerarys.createItinerary($scope.itinerary,function(err,result){
	  		if(err){return console.log(err);}
	  		$state.go('index.editItinerary',{machine_name : result.machine_name, id : result._id});
	  	})
	  }

	if($state.params.machine_name && $state.params.id){
		itinerarys.getItinerary($state.params.machine_name,$state.params.id,function(err,result){
			if(err){console.log(err);$state.go('index.createItineraryDays'); return;}
			$scope.itinerary = result;
			//set start and end date by default

			$scope.itinerary.start_date = new Date(result.start_date);

			$scope.itinerary.trip_to[$scope.itinerary.trip_to.length -1].end_date = new Date(result.end_date);
			
			$scope.itinerary.trip_to.forEach(function(destination){
				if(destination.start_date){
					destination.start_date = new Date(destination.start_date);
				}
				if(destination.end_date){
					destination.end_date = new Date(destination.end_date);
				}
			})
			console.log("$scope.itinerary.trip_to",$scope.itinerary);
			//create days json 
			if(!result.days || result.days && result.days.length ==0 ){
				var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
				var days = diff/(24*60*60*1000);
				$scope.total_days = days;

				$scope.itinerary.days={};

				for(i=0;i<days;i++){
					$scope.itinerary.days[i+1]=[];
				}
			}
		})
	}


}]);