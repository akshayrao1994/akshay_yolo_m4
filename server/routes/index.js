/*
* Index file for all urls management
*/
module.exports = function(app) {
	app.use('/api/users' , require('./users'));
	app.use('/api/attractions' , require('./attractions'));
	app.use('/api/install' , require('./install'));
	app.use('/api/destinations' , require('./destinations'));
	app.use('/api/categories' , require('./categories'));
	app.use('/api/utils' , require('./utils'));
	app.use('/api/itinerarys' , require('./itinerarys'));
	app.use('/api/tags' , require('./tags'));
	// app.use('/api/flights' , require('./flights'));
	app.use('/api/journals' , require('./journals'));
	app.use('/api/comments' , require('./comments'));
}