var express = require('express'),
	router = express.Router(),
	comments_db = require('../libs/comments/comments_db'),
	comments_api = require('../libs/comments/comments_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	dbconfig = require('../config/global_config'),
	_ = require("underscore");


router.get('/all/count',function(req,res){
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}
	comments_api.commentsCount(Query,function(err,data){
		if(err){return  res.status(400).send(err);}
		return res.status(200).json({count : data});
	})
})

router.get('/all',function(req,res){
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}
	comments_api.getall(Query ,function(err,result){
        if (err) {return res.status(400).send(err);}
        return res.status(200).send(result);
    })
})

router.delete('/:id', function(req, res) {
    comments_api.deleteComments(req.params.id, function(err, data) {
        if (err) {return res.status(400).send(err);}
        return res.status(200).send("Journal Deleted");
    })
})

router.post('/create', function(req, res) {

   	if(!req.body.description){
    	return res.status(200).send("Please send description");
    }  

    if(req.user && req.user._id){

        	var comments ={};
    		comments = req.body;
    		comments.created_by = req.user._id;

    	comments_api.checkIfExist(req.body.journal, function(err, result) {
    	if(err){return res.status(400).send(err);}

    	if(result.length == 0){
    		return res.status(400).send('try again with a valid id');
    	}
    	else{
        comments_api.createComments(comments, function(err, result) {
            if (err) {return res.status(400).send(err);}
            return res.status(200).send(result);
        });
    }
    })
}
})

router.get('/:id',function(req,res){
	comments_api.getComments(req.params.id,function(err,data){
		if(err){return  res.status(400).send({message : err});}
		return res.status(200).send(data);
	})
})

router.put('/:id', function(req, res) {
	comments_api.getComments(req.params.id,function(err, data) {

		if(!req.user.isadmin && !req.user.issuperadmin){
			return res.status(400).send('Permission Denied');
		}

/*		if(req.user._id.toString() == data.created_by.toString() ){*/
			req.body.publish ? data.publish = req.body.publish : null;
	        req.body.description ? data.description = req.body.description : null;
	        _.extend(data,req.body);
/*		}*/

				comments_api.updateComments(data, function(err, data) {
		           if (err) {return res.status(400).send(err); }
		            res.status(200).send(data);
		        })
		});
	});



































module.exports =router;