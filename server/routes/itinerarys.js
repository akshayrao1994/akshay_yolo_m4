/*
* All Itinerarys Routes after /api/Itinerarys
*/
var express = require('express'),
	router = express.Router(),
  itinerarys_api = require('../libs/itinerarys/itinerarys_api'); 
var  global_config = require('../config/global_config');
var async = require("async");
var request = require("request");

router.post("/create",function(req,res){
	console.log("req",req.body);
	if(!req.body.trip_from || !req.body.trip_to || !req.body.start_date || !req.body.end_date || !req.body.title || !req.body.machine_name){
		return res.status(400).send({message  : "Required Feild Missing"});
	}

	if(req.body.trip_from._id ){
		req.body.trip_from = req.body.trip_from._id
	}


	if(req.body.trip_to[0]._id || (req.body.trip_to[0].destination && req.body.trip_to[0].destination._id)) {
		
		var new_trip_to = [];
		
		req.body.trip_to.forEach(function(destination){
			var json = {};
			destination._id ? json.destination = destination._id : null;
		
			destination.destination && destination.destination._id ? json.destination = destination.destination._id : null;

			destination.start_date ? json.start_date = destination.start_date : null;
			destination.end_date ? json.end_date = destination.end_date : null;
			
			new_trip_to.push(json);
		})
		req.body.trip_to = new_trip_to;
	}
	req.body.created_by = req.user._id;

	console.log("dddd",req.body);
	
	itinerarys_api.createItinerary(req.body,function(err,results){
		if(err){return res.status(400).send(err);}
	console.log("dddd2",results);
		return res.status(200).send(results);
	})

})

router.get("/:machine_name/:id",function(req,res){
	itinerarys_api.getItinerary(req.params.id,req.params.machine_name,function(err,results){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(results);
	})
})


router.put("/:id",function(req,res){
	req.body.machine_name ?  delete req.body.machine_name : null;
	itinerarys_api.updateItinerary(req.params.id,req.body,function(err,results){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(results);
	})
})

router.delete("/:id",function(req,res){
	itinerarys_api.deleteItinerary(req.params.id,function(err,results){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(results);
	})
})

router.get("/distance",function(req,res){
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}

	if(!Query || !Query.trip_from|| !Query.trip_to){
		return res.status(400).send({message : "From or To Parameters Missing"});
	}
	
	itinerarys_api.googleApis(Query,function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})

})

router.post("/daydistancetime",function(req,res){
	if(!req.body.day){
		return res.status(200).send({message:"Please pass day's JSON"})
	}

	var dayJSON = req.body.day;
	for(var i = 1 ; i < dayJSON.length; i ++){
		var url = "https://maps.googleapis.com/maps/api/directions/json?origin="+dayJSON[i-1].latitude+","+dayJSON[i-1].longitude+"&destination="+dayJSON[i].latitude+","+dayJSON[i].longitude;
		dayJSON[i-1].next_distance_url = url;
	}

async.map(dayJSON, function(attraction, callback) {
    if(attraction.next_distance_url){
	    var url = attraction.next_distance_url;
	    attraction.next_travel_mode ? url += "&mode="+attraction.next_travel_mode :null; 
	    // iterator function
	    request(url + "&key="+global_config.googleApisKey, function (error, response, body) {
	        if (!error && response.statusCode == 200) {
	            var body = JSON.parse(body);
	            // do any further processing of the data here
	            attraction.next_distance = body.routes[0].legs[0].distance;
	            attraction.next_duration = body.routes[0].legs[0].duration;
	            attraction.next_travel_mode = body.routes[0].legs[0].steps[0].travel_mode;
	            callback(null, attraction);
	        } else {
	            callback(error || response.statusCode);
	        }
	    });
    }else{

    	   callback(null, attraction);
    }
}, function(err, results) {
        for(var i=0;i<results.length ; i++){
	 		//set start time variable if not create own 8am start time
        	if(!results[i].start_time){
        	 results[i].start_time =  "08:00";
        	}else{
        		 results[i].start_time = results[i].start_time;
        	}

        	!results[i].stay_time ? results[i].stay_time =  "02:00" : results[i].stay_time = results[i].stay_time;
 
        	console.log("results i----",results[i]);
        	//convert start and stay time in milisecond for time checking
        	
        	var start_time =  results[i].start_time.split(":");
        	var stay_time =  results[i].stay_time.split(":");
        	var newtime =[];
        	newtime[0] = Number(start_time[0])+ Number(stay_time[0]);
        	newtime[1] = Number(start_time[1])+ Number(stay_time[1]);
        	console.log("new time ", newtime);
        	
        	if(results[i].next_duration){
				newtime[1]  = Number(newtime[1]) +  Math.floor(Number(results[i].next_duration.value) /60);        		
        	}

        	console.log("new time2 ", newtime);
        	if(newtime[1] > 60){
        		newtime[0] = Number(newtime[0]) + Math.floor(Number(newtime[1]) / 60);
        		newtime[1] = newtime[1] % 60;
        	}
        	console.log("new time3 ", newtime);

			if(newtime[0] > 23){
				 return res.status(400).send({message :"Oops! There is not enough time for this place on this day"});
			}
 			if(results[i +1]){
 					results[i +1].start_time = newtime.join(":");
 					results[i +1].stay_time = results[i +1].stay_time ? results[i +1].stay_time :  '02:00' ;	
        	}
        }

       return res.status(200).send(results);
});

})



router.post("/getallmodes",function(req,res){
	var modes = ["driving","walking","bicycling","transit"];
	if(!req.body.attraction){
		return res.status(400).send("attraction required for getting all modes");
	}
    if(!req.body.attraction.next_distance_url){
		return res.status(400).send("Next Url Missing  in attraction ");
    }
	var attraction  = req.body.attraction;
	var allmodes = {};
	modes.forEach(function(mode){
		allmodes[mode] = {};
	})
async.map(modes, function(mode, callback) {

	    // iterator function
	    request(attraction.next_distance_url+"&mode="+mode+"&key="+global_config.googleApisKey, function (error, response, body) {
	        if (!error && response.statusCode == 200) {

	            var body = JSON.parse(body);
	            // do any further processing of the data here

	            allmodes[mode].start_address = body.routes[0].legs[0].start_address;
	            allmodes[mode].end_address = body.routes[0].legs[0].end_address;
	            allmodes[mode].next_distance = body.routes[0].legs[0].distance;
	            allmodes[mode].next_duration = body.routes[0].legs[0].duration;
	            allmodes[mode].next_travel_mode = body.routes[0].legs[0].steps[0].travel_mode;
	            allmodes[mode].steps = body.routes[0].legs[0].steps;
	            callback(null, allmodes[mode]);
	        } else {
	            callback(error || response.statusCode);
	        }
	    });
},function(err, results) {
       console.log("-----------------------------",allmodes);
       return res.status(200).send(allmodes);
});

});

router.post('/route-optimizer', function(req, res) {
  var tsp = require('tspsolver');
if(!req.body.day){
	return res.status(400).send("please pass attraction array in day");
}
	req.body.day.forEach(function(day){
  		tsp.addAddress(day.address);
		
	})

  console.log("s",req.body.day)
tsp.solveAtoZ(function() {
      var embed_uri = tsp.createGoogleLink(true);
      var map_uri   = tsp.createGoogleLink();

      var send = {
          'addresses': tsp.addAddress,
          'embed_uri': embed_uri,
          'map_uri' :  map_uri
      };

      console.log(send);
      res.send(send);
  })
});

module.exports =router;

