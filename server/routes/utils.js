/*
* Utility Routes
*/
var express = require('express'),
	router = express.Router(),
	language  = require('../config/language'),
	countries  = require('../config/countries');

router.get('/languages',function(req,res){
	return res.status(200).send(language);
})

router.get('/countries',function(req,res){
	return res.status(200).send(countries);
})

module.exports =router;