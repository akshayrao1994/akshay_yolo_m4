var itinerarys_db = require('./itinerarys_db');
var  global_config = require('../../config/global_config');
var request = require('request');
module.exports = {
	//call goole apis for distance 
	googleApis: function(options,cb){
		var url = "https://maps.googleapis.com/maps/api/directions/json?origin="+options.trip_from+"&destination="+options.trip_to ;
		options.mode ?url = url + "&mode="+options.mode : null;
		options.departure_time ?url = url + "&departure_time="+options.departure_time : null;
		url = url + "&key="+global_config.googleApisKey;
		request(url, function(error, response, body){
			if(error){return cb(error,null)}
			body = JSON.parse(body);
			cb(null,body)
		});
	},
	createItinerary : function(itinerary,cb){
		var filter = {};
		itinerary._id ? filter._id = itinerary._id :null;

		itinerarys_db.findOneAndUpdate(filter,itinerary,{upsert :true, new: true}).populate("trip_from").populate("trip_to.destination").exec(function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},

	getItinerary : function(id,machine_name,cb){
		itinerarys_db.findOne({_id : id , machine_name : machine_name }).populate("trip_to.destination").populate("trip_from").exec(function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},
	updateItinerary : function(id,json,cb){
		itinerarys_db.findOneAndUpdate({_id : id },json).populate("trip_to.destination").populate("trip_from").exec(function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},
	deleteItinerary : function(id,cb){
		itinerarys_db.remove({_id : id },function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}

}