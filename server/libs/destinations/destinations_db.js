// load the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
// define the schema for our destinations model
var destinationsSchema = mongoose.Schema({
    city : String,
    country : String,
    image: String,
    description: String,
    type: String,
    location:{}
});


module.exports = mongoose.model('destinations', destinationsSchema);