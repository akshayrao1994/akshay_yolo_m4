var attractions_db = require('./attractions_db');
var reviews_db = require('../reviews/reviews_db');

module.exports = {

	/**
	 * Create Attraction By Import
	 */
	createAttractionImport: function(attraction, cb) {
		attractions_db.findOneAndUpdate({id:attraction.id},attraction,{upsert :true},function(err, result) {
	        if (err) {return cb(err, null);}
	        return cb(null, result);
	    })
	},

	getAllAttractions : function(query, cb) {
        var skiplimit = {};
        var sort = null;
        query.filter ? filter = query.filter : filter = {};
        if (query.name) {
            filter["name"] = new RegExp(query.name, 'i');
        }
        query.fields ? fields = query.fields :fields={};
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        console.log(query);
        if (sort !== null) {
            attractions_db.find(filter, fields, skiplimit).sort(sort).populate('city').populate('category').exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        }else{
            attractions_db.find(filter, fields, skiplimit).populate('city').populate('category').exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        }
    },
    
    getAllAttractionsCount : function(query,cb) {
        query.filter ? filter = query.filter : filter={};
        attractions_db.find(filter).count().exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },
    getAttraction : function(id , cb) {
        attractions_db.findOne({_id : id}).populate('city').populate('category').exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },
    createAttraction : function(attraction,cb){
        attractions_db.create(attraction,function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },
    updateAttraction : function(id, attraction,cb){
        attractions_db.findOneAndUpdate({_id : id},attraction,function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },
    deleteAttraction : function(id,cb){
        attractions_db.remove({_id : id , id : {$exists : false} },function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },
    updateReviews : function(id,data,cb){
        console.log('finally',id,data);
        reviews_db.findOne({"attraction_id":id,"reviews_data.user_id":data.user_id},function(err,result){
            if (err) {
                console.log(err);
                return cb(err, null);
            }
            console.log('result',result);
/*            if(result.length > 0){
                reviews_db.update({"attraction_id":result._id,"reviews.user_id":result.reviews.user_id},{$set:{'reviews.$.rating':data.rating}})
            }
            else if(result.length == 0){
                reviews_db.insert({"attraction_id":data._id,"reviews.user_id":result.reviews.user_id},{$push:{"reviews":data}})
            }*/
        })
    }
}