var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var tagsSchema = mongoose.Schema({
	name : String
});

module.exports = mongoose.model('tags', tagsSchema);