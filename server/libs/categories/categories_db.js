// load the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
// define the schema for our Attraction model
var categoriesSchema = mongoose.Schema({
    name : String,
    title : String,
});


module.exports = mongoose.model('categories', categoriesSchema);